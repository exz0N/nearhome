var gulp = require('gulp');
var plumber = require('gulp-plumber');
var wiredep = require('wiredep').stream;
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-clean-css');

gulp.task('bower', function () {
    gulp.src('./app/index.html')
        .pipe(wiredep({
            directory: './app/bower_components'
        }))
        .pipe(gulp.dest('./app'));
});

// local server
gulp.task('serve', function() {
    browserSync({
        server: {
            baseDir: "app"
        }
    });
});
gulp.task('bs-reload', function() {
    browserSync.reload();
});

gulp.task('script', function() {
    return gulp.src([
        'app/js/app.js',
        'app/js/routes.js',
        'app/js/services/**/*.js',
        'app/js/controllers/**/*.js',
        'app/js/directives/**/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: function(error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(ngAnnotate())
        .pipe(concat('common.js'))
        .pipe(gulp.dest('app/js'))
        .pipe(browserSync.reload({stream:true}));
});

// compile styles
gulp.task('styles', function() {
    gulp.src(['app/scss/common.scss'])
        .pipe(plumber({
            errorHandler: function(error) {
                console.log(error.message);
                this.emit('end');
            }}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('app/css/'))
        .pipe(browserSync.reload({stream:true}));
});

//Move views to dis folder
gulp.task('move-views', function() {
    return gulp.src('app/views/**/*')
        .pipe(gulp.dest('dist/views/'));
});

//move img to dist folder
gulp.task('move-img', function() {
    return gulp.src('app/img/**/*')
        .pipe(gulp.dest('dist/img/'));
});

//move fonts
gulp.task('move-fonts', function() {
    return gulp.src('app/bower_components/roboto-fontface/fonts/**/*')
        .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('move-favicons', function() {
    return gulp.src([
        'app/apple-touch-icon.png',
        'app/favicon-16x16.png',
        'app/favicon-32x32.png',
        'app/favicon.ico'

    ])
        .pipe(gulp.dest('dist/'));
});

// ---------- BUILD ------------- //
gulp.task('html', ['move-views', 'move-img', 'move-fonts', 'move-favicons'], function () {
    return gulp.src('app/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('dist'));
});

// default
gulp.task('default', ['serve'], function() {
    gulp.watch('bower.json', ['bower']);
    gulp.watch('app/js/**/*.js', ['script']);
    gulp.watch("app/scss/**/*.scss", ['styles']);
    gulp.watch("app/**/*.html", ['bs-reload']);
});