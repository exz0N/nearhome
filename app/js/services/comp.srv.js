angular.module('NearHome')
    .factory('factory', ['baseURL', '$http', function(baseURL, $http) {
        return {
            getCompanies : function() {
                return $http.get(baseURL + 'companies');
            }
        }
    }]);