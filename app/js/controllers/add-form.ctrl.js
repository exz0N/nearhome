angular.module('NearHome')
.controller('AddFormCtrl', ['$scope', 'baseURL', '$http', function ($scope, baseURL, $http) {
    $scope.companyData = {};
    $scope.addCompany = function(e) {
        e.preventDefault();
        $http({
            method : "POST",
            url : baseURL+"companies",
            data : JSON.stringify(this.companyData)
        }).success(function(resp) {
            console.log(resp);
        });
    };

}]);