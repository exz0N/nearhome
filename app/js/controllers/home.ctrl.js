angular.module('NearHome')
.controller('HomeCtrl', ['$scope', '$timeout', '$http', 'baseURL', 'companiesData', function ($scope, $timeout, $http, baseURL, companiesData) {

    GoogleMapsLoader.LANGUAGE = 'uk';
    GoogleMapsLoader.KEY = 'AIzaSyCjr_U8w7Ol-JUZTF0b0sk6Y2-KeIVZzRI';

    $scope.companies = companiesData.data;
    $scope.map = {};
    $scope.markers = [];
    $scope.infoWindows = [];
    $scope.activemarker = '';
    $scope.activeCompany = -1;
    $scope.activeVacancy = -1;
    $scope.showAdditional = false;
    $scope.names = [];

    function scrollTo(element, to, duration) {
        if (duration <= 0) return;
        var difference = to - element.scrollTop;
        var perTick = difference / duration * 10;

        setTimeout(function() {
            element.scrollTop = element.scrollTop + perTick;
            if (element.scrollTop === to) return;
            scrollTo(element, to, duration - 10);
        }, 10);
    }

    function closeAllInfoWindows(window) {
        // if info window not current = close others
        angular.forEach($scope.infoWindows, function (infoWindow, index) {
            if (infoWindow != window) infoWindow.close();
        });
    }

    function scrollToIndex(index) {
        setTimeout(function () {
            var bar = document.querySelectorAll('.accordion')[0];
            var inte = document.querySelectorAll('.accordion__item')[index].offsetTop;
            scrollTo(bar, inte - 100, 200);
            // bar.scrollTop = inte - 100;
        }, 500);
    }

    function geoCode(company) {
        var geocoder = new google.maps.Geocoder();

        if (!company.lat) {
            //no coords yet
            geocoder.geocode(
                {
                    address: company.address + ', ' + company.city,
                    language: 'ua',
                    componentRestrictions: {country: 'UA'}
                }
                , function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {

                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            title: company.name,
                            label: company.name.split('')[0],
                            position: results[0].geometry.location
                        });
                        marker.set("company", company.name);

                        $http({
                            method: "PUT",
                            url: baseURL + 'companies/' + company._id,
                            data: {lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng()}
                        });


                        var infoWindow = new google.maps.InfoWindow({
                            content: '<h4>' + company.name + '</h4> ' +
                            '<a target="_blank" class="company-url" href="' + company.site + '">Сайт компанії</a>' +
                            ' <p>' + company.address + '</p>'
                        });

                        google.maps.event.addListener(marker, 'click', function () {
                            $scope.map.panTo(marker.getPosition());
                            closeAllInfoWindows(infoWindow);
                            infoWindow.open($scope.map, marker);

                            var filteredNames = []; //need to update filtered items each time

                            angular.forEach($scope.filtered, function (item, index) {
                                filteredNames.push(item.name);
                            });

                            //set active compaby in sidebar
                            if ($scope.activeCompany != filteredNames.indexOf(marker.company)) {
                                $timeout(function () {
                                    $scope.activeCompany = filteredNames.indexOf(marker.company);
                                    scrollToIndex(filteredNames.indexOf(marker.company));
                                });

                            }

                        });

                        $scope.names.push(company.name); //collect all company names
                        $scope.markers.push(marker);
                        $scope.infoWindows.push(infoWindow);

                    } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                        //fix to avoid query limit error for multiple requests in same time
                        setTimeout(function () {
                            geoCode(company);
                        }, 300);
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
        } else {
            // console.log('aa', company.lat, company.lng);
            var markerPosition = {lat: parseFloat(company.lat), lng: parseFloat(company.lng)};

            if ($scope.markers.length != 0) {

                angular.forEach($scope.markers, function (marker, index) {
                    //check if already have marker with same pos

                    var pos = marker.getPosition();
                    if (pos.lat() == markerPosition.lat && pos.lng() == markerPosition.lng) {
                        var newLat = markerPosition.lat + (Math.random() - .5) / 1500;
                        var newLng = markerPosition.lng + (Math.random() - .5) / 1500;
                        markerPosition = {lat: newLat, lng: newLng};
                    }
                });

            }

            var marker = new google.maps.Marker({
                map: $scope.map,
                title: company.name,
                label: company.name.split('')[0],
                position: markerPosition
            });
            marker.set("company", company.name);


            var infoWindow = new google.maps.InfoWindow({
                content: '<h4>' + company.name + '</h4> <a target="_blank" class="company-url" href="' + company.site + '">Сайт компанії</a> <p>' + company.address + '</p>'
            });

            google.maps.event.addListener(marker, 'click', function () {
                $scope.map.panTo(marker.getPosition());
                closeAllInfoWindows(infoWindow);
                infoWindow.open($scope.map, marker);

                var filteredNames = []; //need to update filtered items each time

                angular.forEach($scope.filtered, function (item, index) {
                    filteredNames.push(item.name);
                });

                //set active compaby in sidebar
                if ($scope.activeCompany != filteredNames.indexOf(marker.company)) {
                    $timeout(function () {
                        $scope.activeCompany = filteredNames.indexOf(marker.company);
                        scrollToIndex(filteredNames.indexOf(marker.company));
                    });

                }

            });

            $scope.names.push(company.name); //collect all company names
            $scope.markers.push(marker);
            $scope.infoWindows.push(infoWindow);
        }

    }

    $scope.newOpenMarker = function (name) {
        //find marker by custom added property
        function index() {
            var res = '';
            angular.forEach($scope.markers, function (marker, index) {
                if (marker.company == name) {
                    res = index;
                }
            });

            return res;
        }

        $timeout(function() {
            if($scope.activeCompany != -1) google.maps.event.trigger($scope.markers[index()], 'click');
        },0)

    };


    GoogleMapsLoader.load(function (google) {
        //all map func goes here

        $scope.map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 50.4501, lng: 30.523400000000038},
            language: 'uk',
            mapTypeControl: false,
            zoom: 13
        });


        angular.forEach($scope.companies, function (company, index) {
            geoCode(company);
        });

        var clusterOptions = {
            maxZoom: 11,
            gridSize: 30,
            zoomOnClick: true,
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
        };


        //GROUP MARKERS!!!!!!!!!!
        $scope.markerCluster = new MarkerClusterer($scope.map, $scope.markers, clusterOptions);

    });


    $scope.setActive = function (comp, vac) {
        $scope.activeVacancy = -1;
        if (vac || vac == 0) {
            $scope.activeVacancy = vac;
        }

        if ($scope.activeCompany != comp) {
            $scope.activeCompany = comp;
            $scope.showAdditional = false;
        } else {
            if (!vac && vac != 0) {
                $timeout(function () {
                    $scope.activeCompany = -1;
                    $scope.showAdditional = false;
                });
            }
        }

    };

    $scope.additionalInfo = function (url) {
        $http.get(url).success(function (resp) {

            var doc = new DOMParser().parseFromString(resp, 'text/html');
            doc.querySelectorAll('.reply ')[0].remove();
            doc.querySelectorAll('.social-likes')[0].remove();
            doc.querySelectorAll('.sh-info')[0].remove();
            var description = doc.querySelectorAll('.l-vacancy')[0].innerHTML;
            var title = doc.querySelectorAll('.g-h2')[0].innerHTML;
            var showOnDou = '<a class="view-dou" target="_blank" href="' + url + '">Переглянути на DOU</a>';
            var additionalInfoBox = document.querySelectorAll('.additional-info')[0].querySelectorAll('.content')[0];
            additionalInfoBox.innerHTML = '';
            additionalInfoBox.innerHTML = title + description + showOnDou;
            document.querySelectorAll('.additional-info')[0].scrollTop = 0;
            $scope.showAdditional = true;

        });
    };


    $scope.updateMarkers = function () {
        $timeout(function () {
            var companies = [];
            angular.forEach($scope.filtered, function (company, index) {
                companies.push(company.name);
            });

            angular.forEach($scope.markers, function (marker, index) {
                if (companies.indexOf(marker.company) == -1) {
                    marker.setVisible(false);
                } else {
                    marker.setVisible(true);
                }
            });

        })

    };

    $scope.keyCheck = function (e) {
        if(e.code == 'Backspace') $scope.activeCompany = -1;
    }


}]);