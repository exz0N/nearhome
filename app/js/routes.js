angular.module('NearHome')
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('home', {
                url : '/',
                title: 'Головна',
                templateUrl : '../views/home.html',
                controller: 'HomeCtrl',
                resolve : {
                    companiesData : function(factory) {
                        return factory.getCompanies();
                    }
                }
            })
            .state('about', {
                url : '/about',
                title: 'Про сервіс',
                template : "<h1>About</h1>"
            })
            .state('addCompany', {
                url : '/add',
                templateUrl : '../views/add-company.html',
                controller : 'AddFormCtrl'
            })
    });