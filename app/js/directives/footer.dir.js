angular.module('NearHome')
    .directive('footerview', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '../views/footer.html'
        };
    });