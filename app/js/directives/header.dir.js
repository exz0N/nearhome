angular.module('NearHome')
    .directive('headerview', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '../views/header.html'
        };
    });