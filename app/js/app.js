var app = angular.module("NearHome", ['ui.router']);
app.config(["$locationProvider", function($locationProvider) {
    $locationProvider.html5Mode(true);
}]);

app.run(['$rootScope', function($rootScope) {
    $rootScope.$on('$stateChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.title;
    });
}]);
app.constant("baseURL", "http://localhost:3000/");


//custom filter
app.filter('rmNbsp', function () {
    //remove nbsp;
    return  function (text) {
        return text.replace(/&nbsp;/g, ' ');
    }
});