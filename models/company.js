var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var vacanciesSchema = new Schema({
    vacancyTitle : {
        type: String,
        required: true
    },
    vacancyDescription: {
        type: String
    },
    vacancySalary : {
        type: String
    },
    vacancyDouLink : {
        type: String
    },
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

var companySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    douId : {
        type : String,
        required : true,
        unique: true
    },
    lat : {
        type : String
    },
    lng : {
        type : String
    },
    city : {
        type: String,
        required: true
    },
    address : {
        type: String,
        required: true
    },
    site : {
        type : String
    },
    image: {
        type: String
    },
    featured : {
        type : Boolean,
        default : false
    },
    description: {
        type: String
    },
    vacancies: [vacanciesSchema]
}, {
    timestamps: true
});

var Companies = mongoose.model('Company', companySchema);

module.exports = Companies;