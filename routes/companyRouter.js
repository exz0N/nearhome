var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Companies = require('../models/company');
var Verify = require('./verify');

router.use(bodyParser.json());

router.route('/')

    .get(function(req,res,next){
        Companies.find({$where: "this.vacancies.length > 0"})
            .sort({name: 1})
            .exec(function(err, company) {
            if (err) next(err);

            res.json(company);
        });
    })

    .post(function(req, res, next){ //Verify.verifyOrdinaryUser, Verify.verifyAdmin,
        Companies.create(req.body, function(err, company) {
            if (err) return res.status(500).send(err);

            console.log('company created');

            var id = company._id;
            res.writeHead(200, {'Content-Type' : 'text/plain'});

            res.end('Added new company with id ' + id);
        });
    })

    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req, res, next){
        Companies.remove({}, function (err, resp) {
            if (err) throw err;
            res.json(resp);
        });
    });

router.route('/:companyId')

    .get(function(req,res,next){
        Companies.findById(req.params.companyId)
            .exec(function(err, company) {
            if (err) next(err);

            res.json(company);
        });
    })

    .put(function(req, res, next){
        Companies.findByIdAndUpdate(req.params.companyId, {
            $set : req.body
        }, {
            new : true
        }, function(err, company) {
            if (err) throw err;

            res.json(company);
        });
    })

    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req, res, next){
        Companies.findByIdAndRemove(req.params.companyId, function (err, resp) {
            if (err) throw err;

            res.json(resp);
        });
    });

router.route('/:companyId/vacancies')
    .get(function (req, res, next) {
        Companies.findById(req.params.companyId)
            .exec(function (err, company) {
            if (err) next(err);
            res.json(company.vacancies);
        });
    })

    .post(Verify.verifyOrdinaryUser, function (req, res, next) {
        Companies.findById(req.params.companyId, function (err, company) {
            if (err) next(err);

            req.body.postedBy = req.decoded._id;

            company.vacancies.push(req.body);
            company.save(function (err, company) {
                if (err) throw err;
                console.log('Updated Vacancy!');
                res.json(company);
            });
        });
    })

    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin,function (req, res, next) {
        Companies.findById(req.params.companyId, function (err, company) {
            if (err) throw err;
            for (var i = (company.vacancies.length - 1); i >= 0; i--) {
                company.vacancies.id(company.vacancies[i]._id).remove();
            }
            company.save(function (err, result) {
                if (err) throw err;
                res.writeHead(200, {
                    'Content-Type': 'text/plain'
                });
                res.end('Deleted all vacancies!');
            });
        });
    });

router.route('/:companyId/vacancies/:vacancyId')
    .get(Verify.verifyOrdinaryUser, function (req, res, next) {
        Companies.findById(req.params.companyId)
            .populate('vacancies.postedBy')
            .exec(function (err, company) {
            if (err) throw err;
            res.json(company.vacancies.id(req.params.vacancyId));
        });
    })

    .put(Verify.verifyOrdinaryUser, Verify.verifyAdmin,function (req, res, next) {
        // We delete the existing vacancy and insert the updated
        // vacancy as a new vacancy
        Companies.findById(req.params.companyId, function (err, company) {
            if (err) throw err;
            company.vacancies.id(req.params.vacancyId).remove();

            req.body.postedBy  = req.decoded._id;
            company.vacancies.push(req.body);
            company.save(function (err, company) {
                if (err) throw err;
                console.log('Updated vacancies!');
                res.json(company);
            });
        });
    })

    .delete(Verify.verifyOrdinaryUser, function (req, res, next) {
        Companies.findById(req.params.companyId, function (err, company) {

            if(company.vacancies.id(req.params.vacancyId).postedBy != req.decoded._id){
                var errDelComment = new Error('Not auth to del comment');
                errDelComment.status = 403;
                return next(errDelComment);
            }

            company.vacancies.id(req.params.vacancyId).remove();
            company.save(function (err, resp) {
                if (err) throw err;
                res.json(resp);
            });
        });
    });

module.exports = router;